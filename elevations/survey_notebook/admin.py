from django.contrib import admin
from .models import Survey_Notebook, Survey_images,Survey_elevations

# Register your models here.
admin.site.register(Survey_Notebook)
admin.site.register(Survey_elevations)
admin.site.register(Survey_images)
