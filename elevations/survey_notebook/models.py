from django.utils import timezone
from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


def user_directory_path(instance, filename):
    # instance of Survey_images model
    # file will be uploaded to MEDIA_ROOT / userid/<filename>
    return 'Survey/{0}/{1}'.format(instance.note_id.user_id, filename)




class Survey_Notebook(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,default=User)
    Identification = models.CharField(max_length=50, null=True, blank=True,default=None)  # foundations/structures names
    notes = models.CharField(max_length=500, null=True, blank=True,default=None)  # general notes
    date = models.DateField(default=timezone.now(), null=True)
    BenchMark = models.FloatField(null=True, blank=True,default=None)
    Shot = models.FloatField(null=True, blank=True,default=None)
    HI = models.FloatField(null=True, blank=True,default=None)

    style_class={}

    def __str__(self):
        return f'{self.user}:{self.pk}'

    def validate_record(self):
        solved_fields = []
        # related fields [ 'BenchMark', 'Shot', 'HI', 'Target', 'EL', 'RR']
        # handle benchmark,Shot,HI logic
        #SOLVE FOR HI
        if self.BenchMark and self.Shot and not self.HI:
            self.HI = self.BenchMark + self.Shot
            # self.style_class['HI'] = 'solved'
            if self.pk in self.style_class.keys():
                old: dict = self.style_class[self.pk]
                old.update({'HI': 'solved'})
                self.style_class[self.pk] = old
            else:
                self.style_class[self.pk] = {'HI': 'solved'}
        else:
            # self.style_class['HI'] = 'unsolved'
            if self.pk in self.style_class.keys():
                old: dict = self.style_class[self.pk]
                old.update({'HI': 'unsolved'})
                self.style_class[self.pk] = old
            else:
                self.style_class[self.pk] = {'HI': 'unsolved'}

        # solve for Shot
        if self.BenchMark and self.HI and not self.Shot:
            self.Shot = self.HI - self.BenchMark
            # self.style_class['Shot'] = 'solved'
            if self.pk in self.style_class.keys():
                old: dict = self.style_class[self.pk]
                old.update({'Shot': 'solved'})
                self.style_class[self.pk] = old
            else:
                self.style_class[self.pk] = {'Shot': 'solved'}
        else:
            # self.style_class['Shot'] = 'unsolved'
            if self.pk in self.style_class.keys():
                old: dict = self.style_class[self.pk]
                old.update({'Shot': 'unsolved'})
                self.style_class[self.pk] = old
            else:
                self.style_class[self.pk] = {'Shot': 'unsolved'}


        if self.Shot and self.HI and not self.BenchMark:
            self.BenchMark = self.HI - self.Shot
            # self.style_class['BenchMark'] = 'solved'
            if self.pk in self.style_class.keys():
                old: dict = self.style_class[self.pk]
                old.update({'BenchMark': 'solved'})
                self.style_class[self.pk] = old
            else:
                self.style_class[self.pk] = {'BenchMark': 'solved'}
        else:
            # self.style_class['BenchMark'] = 'unsolved'
            if self.pk in self.style_class.keys():
                old: dict = self.style_class[self.pk]
                old.update({'BenchMark': 'unsolved'})
                self.style_class[self.pk] = old
            else:
                self.style_class[self.pk] = {'BenchMark': 'unsolved'}
    def get_class(self):
      return self.style_class[self.pk]


class Survey_elevations(models.Model):
    elevation_id = models.ForeignKey(Survey_Notebook, on_delete=models.CASCADE, default=Survey_Notebook)
    name = models.CharField(max_length=50, null=True, blank=True, default=None)  # label ex: TOC,TOB,TOF
    EL = models.FloatField(max_length=6,null=True, blank=True, default=None)
    RR = models.FloatField(max_length=6,null=True, blank=True, default=None)
    HI = None
    style_class = {}
    def __str__(self):
        return f'EL {self.pk} from {self.elevation_id}'

    def validate_record(self):
        solved_fields = []
        # related fields [ 'BenchMark', 'Shot', 'HI', 'Target', 'EL', 'RR']
        # handle benchmark,Shot,HI logic
        if self.EL and self.HI and not self.RR:
            self.RR = round(self.HI - self.EL,2)
            # self.style_class['RR'] = 'solved'
            if self.pk in self.style_class.keys():
                old:dict = self.style_class[self.pk]
                old.update({'RR':'solved'})
                self.style_class[self.pk] = old
            else:
                self.style_class[self.pk] = {'RR':'solved'}

        else:
            # self.style_class['RR'] = 'unsolved'
            if self.pk in self.style_class.keys():
                old:dict = self.style_class[self.pk]
                old.update({'RR':'unsolved'})
                self.style_class[self.pk] = old
            else:
                self.style_class[self.pk] = {'RR':'unsolved'}

        if self.RR and self.HI and not self.EL:
            self.EL = round(self.HI - self.RR,2)
            # self.style_class['RR'] = 'solved'
            if self.pk in self.style_class.keys():
                old:dict = self.style_class[self.pk]
                old.update({'EL':'solved'})
                self.style_class[self.pk] = old
            else:
                self.style_class[self.pk] = {'EL':'solved'}

        else:
            # self.style_class['RR'] = 'unsolved'
            if self.pk in self.style_class.keys():
                old:dict = self.style_class[self.pk]
                old.update({'EL':'unsolved'})
                self.style_class[self.pk] = old
            else:
                self.style_class[self.pk] = {'EL':'unsolved'}




        return self.style_class
    def get_class(self):
      return self.style_class[self.pk]


class Survey_images(models.Model):
    note_id = models.ForeignKey(Survey_Notebook, on_delete=models.CASCADE,default=Survey_Notebook)
    image = models.ImageField(default="no-image.jpg", upload_to=user_directory_path, null=True, blank=True)



# Create your models here.
