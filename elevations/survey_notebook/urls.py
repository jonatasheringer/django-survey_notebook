from django.urls import path
from .views import add_record_view, survey_home_view, view_survey_record_view, \
    handle_actions, update_survey_form_view, manage_survey_images_view, \
    delete_survey_images

urlpatterns = [
    path('', survey_home_view, name='survey_home_view'),
    path('add/', add_record_view, name='add_record_view'),
    path('view/', view_survey_record_view, name='view_survey_record_view'),
    path('action/', handle_actions, name='handle_actions'),
    path('delete/<int:id>', delete_survey_images, name='delete_survey_images'),
    path('update/<int:id>', update_survey_form_view, name='update_survey_form_view'),
    path('images/<int:id>', manage_survey_images_view, name='manage_survey_images_view'),

    # path('update/<int:id>', update_workout_form_view, name='update_workout_form_view'),

]
