from django.forms import ModelForm
from django import forms
from .models import Survey_Notebook, Survey_images, Survey_elevations
from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth.models import User


class CreateSurveyNote(forms.ModelForm):
    class Meta:
        model = Survey_Notebook
        fields = ['user', 'Identification', 'notes', 'date', 'BenchMark', 'Shot', 'HI']
        exclude = ['user']
        widgets = {
            'date': forms.SelectDateWidget(
                empty_label=("Choose Year", "Choose Month", "Choose Day"),
            ),
            'notes': forms.Textarea(attrs={'rows': 3, 'cols': 35}),
        }


class CreateSurveyImages_form(forms.ModelForm):
    class Meta:
        model = Survey_images
        fields = ['note_id', 'image']
        exclude = ['note_id']


class CreateSurveyElevations(forms.ModelForm):
    class Meta:
        model = Survey_elevations
        fields = ['name', 'EL', 'RR', 'id']
        widgets = {
            'name': forms.TextInput(attrs={'style': 'width:70px'}),
            'EL': forms.NumberInput(attrs={'style': 'width:70px'}),
            'RR': forms.NumberInput(attrs={'style': 'width:70px'}),
            "id": forms.HiddenInput(),
        }
