from django.apps import AppConfig


class SurveyNotebookConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'survey_notebook'
