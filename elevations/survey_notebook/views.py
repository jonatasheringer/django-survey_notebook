import datetime

import django.utils.timezone
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import Survey_Notebook, Survey_images, Survey_elevations
from .forms import CreateSurveyNote, CreateSurveyImages_form, CreateSurveyElevations
from django.urls import reverse
import json


# TODO: edit records,make view look good. add date field
# error in template because image dont have a default image to display
def survey_home_view(request, *args, **kwargs):
    return render(request, 'survey_home.html', {})


def handle_actions(request, *args, **kwargs):
    if request.GET:
        print(request.GET)
        if 'delete' in request.GET:
            d = Survey_Notebook.objects.get(user=request.user, pk=request.GET['delete'])
            d.delete()
            return view_survey_record_view(request)
        elif 'template' in request.GET:
            w = Survey_Notebook.objects.get(pk=request.GET['template'])
            form = CreateSurveyNote(instance=w)
            return render(request, 'add_record.html', {"form": form})
        elif 'edit' in request.GET:
            return redirect('update_survey_form_view', id=request.GET['edit'])
        elif 'images' in request.GET:
            node_id = request.GET['images']
            return manage_survey_images_view(request, id=node_id)
            # return redirect('manage_survey_images_view', id=node_id)
        else:
            return view_survey_record_view(request)


def update_survey_form_view(request, id, *args, **kwargs):
    w = Survey_Notebook.objects.get(pk=id)
    form = CreateSurveyNote(instance=w)
    els_forms = []
    elevations = Survey_elevations.objects.filter(elevation_id=id)
    for e in elevations:
        print(f'loading pk{e.pk}| name:{e.name} EL:{e.EL} RR:{e.RR}')
        e_form = CreateSurveyElevations(instance=e)
        els_forms.append([e_form, e.pk])

    if request.POST:
        print(request.POST)
        w = Survey_Notebook.objects.get(pk=id)
        form = CreateSurveyNote(request.POST, instance=w)

        if 'pk' in request.POST:
            pks: list = request.POST.getlist('pk')
            names = request.POST.getlist('name')
            rrs = request.POST.getlist('RR')
            els = request.POST.getlist('EL')
            zipped = zip(pks, names, rrs, els,range(len(pks)))
            toBePopped = []
            toBeAdded = []
            for pk, name, rr, el,index in zipped:
                print(f'start index {index}')
                print(pks)
                if pk == 'new':
                    if rr == '' and el == '' and name == '':  # skips empty records
                        toBePopped.append(index)
                        print(f'skipping index{index} name:{name}')
                        continue

                    else:
                        if rr == '':
                            rr = None
                        if el == '':
                            el = None
                    elevations = Survey_Notebook
                    elevation = elevations.objects.get(pk=id)
                    new = elevation.survey_elevations_set.create(name=name, EL=el, RR=rr)
                    print(f'new elevation added to Note {id}, {new.pk}')
                    toBePopped.append(index)
                    print(f'popping index{index} name:{name} pk:{pk}')
                    toBeAdded.append(new.pk)
                    continue

                elevation = Survey_elevations.objects.get(pk=pk)
                e_form = CreateSurveyElevations(instance=elevation)
                uform = e_form.save(commit=False)
                uform.name = name
                if rr != '':
                    uform.RR = float(rr)
                else:
                    uform.RR = None

                if el != '':
                    uform.EL = float(el)
                else:
                    uform.EL = None
                uform.save()
                print(f'elevation ({pk}) updated ')
            #remove all indexes with 'new' from pks
            for i in sorted(toBePopped, reverse=True):
                pks.pop(i)
                # we reverse the order of indexes to avoid
                # IndexError: pop index out of range:
                # you can pop indexes 9,7,5,3 (decreasing order)
                # but not 3,5,7,9
                # by the time we pop the first 3 indexes the list would be lenght of 6
                # and we would try to pop 9 that no longer exist

            pks.extend(toBeAdded) # add newly created pk's to pks list
            #we delete all elevations not found on pks list
            rows_to_delete = Survey_elevations.objects.exclude(pk__in=pks)
            rows_to_delete.all().delete()
        if form.is_valid():
            # f = form.save(commit=False)
            # f.user = request.user
            form.save()
            # return view_survey_record_view(request)
            return HttpResponseRedirect(reverse('view_survey_record_view'))
        else:
            return render(request, 'update_record.html', {"form": form, "id": id, "els_forms": els_forms})
    return render(request, 'update_record.html', {"form": form, "id": id, "els_forms": els_forms})


def manage_survey_images_view(request, id, *args, **kwargs):
    form = CreateSurveyImages_form(initial={'note_id': id})
    context = {'form': form}
    if request.POST:
        if 'delete' in request.POST:
            d = Survey_images.objects.get(pk=request.POST['delete'])
            d.delete()
        else:
            form = CreateSurveyImages_form(request.POST, request.FILES)
            print('saving image')
            context = {'form': form}
            if form.is_valid():
                f = form.save(commit=False)
                sn = Survey_Notebook.objects.get(pk=id)
                f.note_id = sn
                f.save()
                context['message'] = "image created"
                return HttpResponseRedirect(reverse('manage_survey_images_view',args=[id]))
    else:
        print('not POST')
    img_queryset = Survey_images.objects.filter(note_id=id)

    context['images'] = img_queryset
    context['id'] = id

    return render(request, 'survey_manage_images.html', context)



def delete_survey_images(request, id, *args, **kwargs):
    d = Survey_images.objects.get(pk=id)
    d.delete()
    return manage_survey_images_view(request)


def view_survey_record_view(request, *args, **kwargs):
    records = list(Survey_Notebook.objects.filter(user=request.user).reverse())
    records.reverse()
    data = []
    message = ""
    dev = ""
    if len(records) == 0:
        message = ("no records yet", "warning")
    else:  # we have records, lets extract the image urls

        for record in records:
            images = []
            elevations = []
            record.validate_record()
            for img in record.survey_images_set.all():  # iterate image model inside Survey_Notebook
                images.append(img.image.url)
                # print(img.image.url)
            if len(images) == 0:
                images.append("/media/no-image.jpg")
            for e in record.survey_elevations_set.all():  # iterate image model inside Survey_Notebook
                e.HI = record.HI
                e.validate_record()
                #todo: validation working right, but not outputting class properly
                elevations.append(e)


            data.append([record, images, elevations])  # gather queryset,images,elevations

        style_class = records[-1].style_class
        style_class = json.dumps(style_class,indent=4)
        dev = (f'style_class:\n' \
               f'{style_class}\n' \
               f'All:\n' \
               f'images:\n ' \
               f'{images}',
               "textarea")
    context = {"records": records,
               "message": message,
               "dev": dev,
               "data": data}
    return render(request, 'view_records.html', context)


def add_record_view(request, *args, **kwargs):
    print(f'request:\n\n{request.POST}\n\nend request')
    # todo: default date stop working, template not getting filled.
    form = CreateSurveyNote(request.POST, initial={"user": request.user, "date": django.utils.timezone.now()})
    form1 = CreateSurveyElevations()

    # Two changes are required to save images, 1 is pass request.FILES to form
    # and 2 set enctype='multipart/form-data' inside <form> tag in used template
    if request.POST:
        form = CreateSurveyNote(request.POST, request.FILES, initial={"user": request.user})
        if form.is_valid():
            f = form.save(commit=False)
            f.user = request.user
            f.save()
            pk = f.pk
            print(f'pk={pk}')
            if 'name' in request.POST:
                names = request.POST.getlist('name')
                rrs = request.POST.getlist('RR')
                els = request.POST.getlist('EL')
                zipped = zip(names, rrs, els)
                print(names)
                for name, rr, el in zipped:
                    if rr == '' and el == '' and name == '':  # skips empty records
                        print(f'skipping record')
                        continue

                    else:
                        if rr == '':
                            rr = None
                        if el == '':
                            el = None
                        elevations = Survey_Notebook
                        elevation = elevations.objects.get(pk=pk)
                        print(f'record={elevation}')
                        print(f'record={dir(elevation.survey_elevations_set)}')
                        elevation.survey_elevations_set.create(name=name, EL=el, RR=rr)
                        print(f'new elevation added to Note{pk}')

            else:
                print('no names')

            return HttpResponseRedirect(reverse('view_survey_record_view'))
    return render(request, 'add_record.html', {"form": form, "form1": form1})

# Create your views here.
