import json

import django.contrib.auth
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login, logout
from sh import date, uname


# Create your views here.
def register_form(request, *args, **kargs):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/login')
    context = {"form": form}
    return render(request, "register.html", context)


def login_view(request, *args, **kargs):
    if request.method == 'POST':
        username = request.POST.get("username")
        password = request.POST.get("password")
        print(f'login_view as {username} and {password}')
        user = authenticate(request, username=username, password=password)
        print(f'auth={user}')

        # TODO:  server fails when sending money to a user that doen't have a wallet.
        if user is not None:
            print(f'authenticated as {user}')
            login(request, user)
            return redirect('home')
        else:
            print(f'authentication failed')

            return HttpResponse('Authentication failed!')

    form = AuthenticationForm()
    context = {"form": form}
    return render(request, "login.html", context)


def logout_action(request, *args, **kargs):
    logout(request)
    return redirect('home')


def getinfo():
    return f'{date()} {uname("-o")}'


def home_view(request, *args, **kwargs):
    # t_speak('hello')
    info = getinfo()
    meta = request.META
    meta_string=""
    for i in meta:
       meta_string+=(f'{i} = {meta[i]}\n')

    context = {'info': info,
               'dev': (meta_string, "pre")
               }
    return render(request, "home.html", context)
    # return HttpResponse("this is home")
